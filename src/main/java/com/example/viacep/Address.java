package com.example.viacep;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties
public class Address {

	private String zip;
	private String address;
	private String district;
	private String city;
	private String state;

	public Address() {

	}

	public Address(String zip, String address, String district, String city, String state) {

		this.zip = zip;
		this.address = address;
		this.district = district;
		this.city = city;
		this.state = state;

	}

	@JsonProperty("cep")
	public String getZip() {
		return zip;
	}
	@JsonProperty("cep")
	public void setZip(String zip) {
		this.zip = zip;
	}

	@JsonProperty("logradouro")
	public String getAddress() {
		return address;
	}
	@JsonProperty("logradouro")
	public void setAddress(String address) {
		this.address = address;
	}

	@JsonProperty("bairro")
	public String getDistrict() {
		return district;
	}
	@JsonProperty("bairro")
	public void setDistrict(String district) {
		this.district = district;
	}

	@JsonProperty("localidade")
	public String getCity() {
		return city;
	}
	@JsonProperty("localidade")
	public void setCity(String city) {
		this.city = city;
	}

	@JsonProperty("uf")
	public String getState() {
		return state;
	}
	@JsonProperty("uf")
	public void setState(String state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "Address{" +
				"zip='" + zip + '\'' +
				", address='" + address + '\'' +
				", district='" + district + '\'' +
				", city='" + city + '\'' +
				", state='" + state + '\'' +
				'}';
	}
}
