package com.example.viacep;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;


@RestController
@RequestMapping("/address")
public class AddressController {

    @GetMapping("{zip}")
    public String treatingJson(RestTemplate json, @PathVariable String zip) {

        Address address = json.getForObject(
                String.format("https://viacep.com.br/ws/%s/json", zip),
                Address.class);
        return address.toString();

    }
}
